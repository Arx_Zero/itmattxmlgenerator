const builder = require('xmlbuilder');
const casual = require('casual');
const fs = require('fs');

//data to be used in generated doc
let senderNetworkAddress = 'APG'

let barcode = 'ZZ'+casual.unix_time+'CN'
let nameS = casual.full_name
let nameR = casual.full_name
let senderAddress = casual.address
let recipientAddress = casual.address
let randomString = casual.sentence 



//XML TEMPLATE

var doc = builder.create('interchange').att('xmlns','m33').att('xmlns:xsi','http://www.w3.org/2001/XMLSchema-instance').att('xsi:schemaLocation','m33 m33-11.xsd');

doc.ele('date').txt('201906090200').up()
    .ele('intref').txt('INTREF1325').up()
    .ele('sender_network_address').txt(`${senderNetworkAddress}`).up()
    .ele('sender_address_qualifier').txt('UP').up()
    .ele('recipient_network_address').txt('MT501').up()
    .ele('recipient_address_qualifier').txt('DL').up()
    .ele('message')
        .ele('mesref').txt('MESREF1325').up()
        .ele('msgtype')
            .ele('code').txt('ITMATT').up()
            .ele('version').txt('1').up()
            .ele('release').txt('5').up()
            .ele('usage_level').txt('0').up()
            .ele('variant').up()
        .up()
        .ele('msgbody')
            .ele('itmatt_1_5_0')
                .ele('item')
                    .ele('ID')
                        .ele('value').txt(`${barcode}`).up()
                    .up()
                    .ele('alternate_ID')
                        .ele('idtype').txt('900').up()
                        .ele('value').txt('1095632').up()
                    .up()
                    .ele('service_contractor_party').txt('N/A').up()
                    .ele('attributes_version_number').txt('N/A').up()
                    .ele('parties')
                        .ele('sender')
                            .ele('role').txt('CZ').up()
                            .ele('identification')
                                .ele('name').txt(`${nameS}`).up()
                            .up()
                            .ele('postal-address')
                                .ele('premises').txt(`${senderAddress}`).up()                            
                                .ele('locality')
                                    .ele('code').txt(casual.zip()).up()
                                    .ele('name').txt(casual.city).up()
                                .up()
                                .ele('country_cd').txt(casual.country_code).up()
                            .up()
                            .ele('contact')
                                .ele('email').up()
                                .ele('telephone').txt('11111111').up()
                            .up()
                        .up()
                        .ele('addressee')
                            .ele('role').txt('CN').up()
                            .ele('identification')
                                .ele('name').txt(`${nameR}`).up()
                            .up()
                            .ele('postal-address')
                                .ele('premises').txt(`${recipientAddress}`).up()                            
                                .ele('locality')
                                    .ele('code').txt(casual.zip()).up()
                                    .ele('name').txt('Birzebbugia').up()
                                    .ele('region').txt('N/A').up()
                                .up()
                                .ele('country_cd').txt('MT').up()
                            .up()
                            .ele('contact')
                                .ele('email').up()
                                .ele('telephone').txt('22222222').up()
                            .up()
                        .up()
                    .up()
                    .ele('declared_gross_weight').txt('0.3').up()
                    .ele('postage_paid')
                        .ele('currency').up()
                        .ele('amount').up()
                    .up()
                    .ele('nature_of_transaction_code').txt('11').up()
                    .ele('total_declared_value')
                        .ele('currency').txt('EUR').up()
                        .ele('amount').txt('10').up()
                    .up()
                    .ele('associated_document')
                        .ele('doctype').txt('N/A').up()
                    .up()
                    .ele('content_piece')
                        .ele('number').txt('1').up()
                        .ele('number_of_units').txt('1').up()
                        .ele('description').txt(`${randomString}`).up()
                        .ele('declared_value')
                            .ele('currency').txt('EUR').up()
                            .ele('amount').txt('10').up()
                        .up()
                        .ele('net_weight').txt('0.500').up()
                        .ele('origin_location_code').up()
                    .up()
                    .ele('event')
                        .ele('status').txt('N/A').up()
                        .ele('code').txt('N/A').up()
                        .ele('associated_entity')
                            .ele('entity_type').txt('N/A').up()
                            .ele('ID')
                                .ele('value').txt('N/A').up()
                            .up()
                        .up()
                        .ele('location_function').txt('N/A').up()
                        .ele('location')
                            .ele('code').txt('N/A').up()
                        .up()
                        .ele('latest_date').txt('N/A')


    

//console.log(doc.toString({ pretty: true }));
console.log(barcode)
fs.writeFileSync(`./itmattin_generated_${casual.unix_time}.xml`, doc.toString({ pretty: true }));